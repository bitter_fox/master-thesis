#!/bin/bash

url=$(git remote -v | sed -n -e "1s/^origin\s\+//g; 1s/\:/\//g; 1s/^git@/https:\/\//g; 1s/\.git.\+$//g; 1p;")
comment=$(git log -1 --date=iso --pretty="format:%ad : %s \"$url/commits/%H\"")

curl -F file=@build/master-thesis.pdf -F initial_comment="$comment" -F channels=#times-s-yoshida -F token=$SLACK_TOKEN https://slack.com/api/files.upload | grep -v "\"ok\":false"
